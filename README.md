# Trivia game



## Instructions
Follow the below instructions to run the application

1. Clone the repository to your computer
2. Run `npm install` in the root of the cloned repository
3. Run `npm run serve` to run the application

## Heroku
Got this message when I tried to follow the instructions for deploying the app to heroku.
ERROR: Application not supported by 'heroku/nodejs' buildpack
remote:  !
remote:  !     The 'heroku/nodejs' buildpack is set on this application, but was
remote:  !     unable to detect a Node.js codebase.
 ! [remote rejected] main -> main (pre-receive hook declined)
error: failed to push some refs to 'https://git.heroku.com/trivia-game-sinan.git'

I messed a bit around with creating multiple heroku apps in the beginning and had some weird folder structure so I must have screwed something up I guess
## Missing functionality
Didnt manage to push back the score of the user to the API
