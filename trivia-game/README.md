# Trivia Game
The game allows you to choose category, difficulty and the amount of questions for the game before you start. You then get four alternatives and only one is the right answer. In the end you will be given the results.<br/><br/>

This is a game made with the Vue framwork with javascript.<br/> 
The questions are being fetched from an open API: [https://opentdb.com/api_config.php](https://opentdb.com/api_config.php) <br/>
<br/>



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Contributors:
### Sinan Selboe Karagulle
### Asso Rostampoor
