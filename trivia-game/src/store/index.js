import Vue from 'vue'
import Vuex from 'vuex'
import {loginAPI} from '@/components/Login/LoginAPI'
import {QuestionsAPI} from '@/components/Questions/QuestionsAPI'
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex)
export default new Vuex.Store({
    plugins: [createPersistedState({
        storage: window.sessionStorage,
    })],
    state:{
        username : '',
        profile: {},
        questions:[],
        score: 0,
        currentQuestion: 0,
        error: '',
        answers : [],
        amountQuestions: 0,
        categoryQuestions: 0,
        difficulty:""

    },
    mutations:{
        setUsername: (state,payload) => {
            state.username = payload
        },
        setPassword: (state,payload) => {
            state.password = payload
        },
        setProfile: (state,payload) => {
            state.profile = payload
        },
        setQuestions: (state,payload) => {
            state.questions = payload
        },
        setScore: (state,payload) => {
            state.score = payload
        },
        setCurrentQuestion: (state,payload) => {
            state.currentQuestion = payload
        },
        setError: (state,payload) => {
            state.error = payload
        },
        setAmountQuestions: (state,payload) => {
            state.amountQuestions = payload
        },
        setCategoryQuestions: (state,payload) => {
            state.categoryQuestions = payload
        },
        setDifficulty: (state,payload) => {
            state.difficulty = payload
        },
        addAnswer: (state,payload) => {
            state.answers.push(payload)
        },
        emptyAnswer: (state) =>{
            state.answers = []
        },

    },
    getters:{
        questions: state => state.questions,
        score: state => state.score,
        currentQuestion: state => state.currentQuestion,
        answers: state => state.answers

    },
    actions:{
        /*
        Register the user in the database, checks if it exists. If the user exists it will not
        add a new entry to the api
        */
        async registerUser({state,commit}){
            try{
                /*user always comes back as undefined ?? but it manages to commit to the API
                I therefore cannot get the score of the player*/

              await loginAPI.register(state.username)

            
            }

            catch(error){
                commit('setError',error.message)
            }
        },
        async fetchQuestions({state,commit}){
            try {
                const questions = await QuestionsAPI.fetchQuestions(state.categoryQuestions,state.difficulty,state.amountQuestions)
                commit('setQuestions',questions)
            } catch (error) {
                commit('setError',error.message)
                
            }
        }
    }

})