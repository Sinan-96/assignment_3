import Vue from 'vue'
import store from './store'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import VueRouter from 'vue-router'
import StartPage from "@/components/StartPage";
import QuestionPage from '@/components/QuestionPage.vue'
import ResultPage from '@/components/ResultPage.vue'
//import router from './router'
Vue.use(VueRouter)
const routes = [
    { path: '/question', component: QuestionPage },
    { path: '/result', component: ResultPage },
    { path: '/', component: StartPage }
]
Vue.config.productionTip = false

new Vue({
  router: new VueRouter({routes}) ,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
