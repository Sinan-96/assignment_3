const apiURL = 'https://trivia-game-sinan.herokuapp.com'
const apiKey = 'api_key'

export const loginAPI = {
    async register(userName){
        const requestOptions = {
            method: "POST",
            headers: {"content-Type" : "application/json",'X-API-Key': apiKey},
            body: JSON.stringify({ 
                username: userName, 
                highScore: 0 
            })
        }
        /*Checks if the user is already have an entry in the
        API*/
        const userExists = await checkIfExists(userName)
        if(userExists.length != 0){
            throw new Error('Could not create user, because username is already taken')
        }

        return fetch(`${apiURL}/trivia`,requestOptions)
        .then(response => {
            if (!response.ok) {
              throw new Error('Could not create new user')
            }
            return response.json()
          })
          .then(newUser => {
            // newUser is the new user with an id
            newUser.newUser
          })
          .catch(error => {
              throw error
          })
    }
}

// gets entries with the argument as username
async function checkIfExists(userName){
    return fetch(`${apiURL}/trivia?username=${userName}`)
        .then(response => response.json())
        .then(results => results)
}