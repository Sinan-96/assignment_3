export const QuestionsAPI ={
    fetchQuestions(category,difficulty,amount){
        return fetch(`https://opentdb.com/api.php?amount=${amount}&category=${category}&difficulty=${difficulty}&type=multiple`)
            .then(response => response.json())
            .then(data => data)
    }
}